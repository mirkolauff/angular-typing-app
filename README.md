# Angular Typing App

A simple game in Angular: Type the given random sentence!

## [Check out live demo.](https://angular-typing-app.mirkolauff.now.sh/)

![Example](src/assets/docs/example.gif)

## Local Development

### Requirements

- node >= 12.16

### Installation

    $ npm i

### Running app

    $ npm start

## Made with

[![Angular](src/assets/docs/angular-icon.png)](https://angular.io) [![Bulma](src/assets/docs/bulma-icon.jpg)](https://bulma.io/) [![Now.sh](src/assets/docs/now-sh-icon.png)](https://now.sh/)
